
///1.
let paragraphs = document.querySelectorAll("p");
for (let p of paragraphs) {
  p.style.backgroundColor = '#ff0000';
}
console.log(paragraphs);

///2.
let list = document.querySelector("#optionsList");
console.log(list);

let parent = document.getElementById("optionsList").parentNode;
console.log(parent);

for (key of list.children) {
  console.log(key.nodeType, key.nodeName);
}

///3.
let text = document.querySelectorAll("#testParagraph");
text.forEach((el) => el.textContent = "This is a paragraph");
console.log(text);

///4.
let elem = document.querySelectorAll(".main-header > *");
elem.forEach((el) => el.classList.add("nav-item"));
console.log(elem);

///5.
let del = document.querySelectorAll(".section-title");
del.forEach((el) => el.classList.remove("section-title"));
console.log(del);







